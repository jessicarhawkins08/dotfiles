#!/bin/bash

brew install caskroom/cask/brew-cask

brew cask install alfred
brew cask install arq
brew cask install firefox
brew cask install google-chrome
brew cask install hipchat
brew cask install iterm2
brew cask install java
brew cask install libreoffice
brew cask install phpstorm
brew cask install skype
brew cask install slack
