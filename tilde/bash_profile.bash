# Don't put duplicate lines in the history
export HISTCONTROL=ignoreboth:erasedups

# Set history length
HISTFILESIZE=1000000000
HISTSIZE=1000000

# Append to the history file, don't overwrite it
shopt -s histappend
# Check the window size after each command and, if necessary, update the values of LINES and COLUMNS
shopt -s checkwinsize
# Autocorrect typos in path names when using `cd`
shopt -s cdspell
# Save all lines of a multiple-line command in the same history entry (allows easy re-editing of multi-line commands)
shopt -s cmdhist
# Do not autocomplete when accidentally pressing Tab on an empty line. (It takes forever and yields "Display all 15 gazillion possibilites?")
shopt -s no_empty_cmd_completion

# Do not overwrite files when redirecting using ">". Note that you can still override this with ">|"
set -o noclobber

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
	shopt -s "$option" 2> /dev/null
done

# Locale
export LC_ALL=en_US.UTF-8
export LANG="en_US"

# Set the composer home.
export COMPOSER_HOME="$HOME/.composer"

# Prepend $PATH without duplicates
function _prepend_path() {
	if ! $( echo "$PATH" | tr ":" "\n" | grep -qx "$1" ) ; then
		PATH="$1:$PATH"
	fi
}

# Construct $PATH
PATH="/usr/bin:/bin:/usr/sbin:/sbin"
[ -d /usr/texbin ] && _prepend_path "/usr/texbin"
[ -d /usr/local/sbin ] && _prepend_path "/usr/local/sbin"
[ -d /usr/local/bin ] && _prepend_path "/usr/local/bin"
command -v brew >/dev/null 2>&1 && _prepend_path "$(brew --prefix coreutils)/libexec/gnubin"
[ -d $HOME/bin ] && _prepend_path "$HOME/bin"
[ -d $HOME/dotfiles/bin ] && _prepend_path "$HOME/dotfiles/bin"
[ -d "$COMPOSER_HOME/vendor/bin" ] && _prepend_path "$COMPOSER_HOME/vendor/bin"
_prepend_path "./vendor/bin"
export PATH

# Colors
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="$(tput setaf 3)"
BLUE="$(tput setaf 4)"
MAGENTA="$(tput setaf 5)"
CYAN="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
GRAY="$(tput setaf 8)"
BOLD="$(tput bold)"
UNDERLINE="$(tput sgr 0 1)"
INVERT="$(tput sgr 1 0)"
NOCOLOR="$(tput sgr0)"

# vim is default editor
export EDITOR=`which vim`

# Load prompt and aliases
for file in ~/dotfiles/includes/bash_*.bash; do
	[ -r "$file" ] && source "$file"
done
unset file

if [ `uname` == 'Darwin' ]; then
    export GOROOT="/usr/local/Cellar/go/1.3/libexec"
    export GOPATH=$HOME/go

    launchctl setenv GOROOT $GOROOT
    launchctl setenv GOPATH $GOPATH
fi

# Tell ls to be colourful
export CLICOLOR=1

# Tell grep to highlight matches
export GREP_OPTIONS='--color=auto'

# Make less the default pager, and specify some useful defaults.
less_options=(
	# If the entire text fits on one screen, just show it and quit. (Be more
	# like "cat" and less like "more".)
	--quit-if-one-screen

	# Do not clear the screen first.
	--no-init

	# Like "smartcase" in Vim: ignore case unless the search pattern is mixed.
	--ignore-case

	# Do not automatically wrap long lines.
	--chop-long-lines

	# Allow ANSI colour escapes, but no other escapes.
	--RAW-CONTROL-CHARS

	# Do not ring the bell when trying to scroll past the end of the buffer.
	--quiet

	# Do not complain when we are on a dumb terminal.
	--dumb
);
export LESS="${less_options[*]}"
unset less_options
export PAGER='less'

# Load extra (private) settings
[ -f ~/.bashlocal ] && source ~/.bashlocal

# Force a green prompt
true
